# Solution

## Distance

Distance is calculated as L1 distance or Manhattan Distance which is most appropiated for a grid

## Algorithm

- Consider *points* as a list of coordinates of active location points in the grid
- Consider *threshold* as a maximum distance among points inside the cluster
- calculate *N* as the total amount of *points*
- Initialize *C* as an empty list of clusters
- for *I* in 1...N 
    - Initialize *cluster* as a set containing only the i-th element
    - for *J* in I+1...N:
        - calculate the distance *Dij* between the element I and element J of the points list
        - if *Dij* <= *threshold* then:
            - add to *cluster* the j-th element of the points list
        - if the *cluster* is not a subset of any previous cluster added to *C* then:
            - add *cluster* to *C*
