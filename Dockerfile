FROM python:3.9-slim

RUN set -ex && pip install poetry
RUN set -ex && apt-get update && apt-get install make

COPY pyproject.toml poetry.lock /app/

WORKDIR /app

RUN set -ex && \
    poetry config virtualenvs.create false && \
    poetry install

COPY . /app
