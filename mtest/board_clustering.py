from typing import List, Set

from pydantic import BaseModel


class Point(BaseModel):
    x: int
    y: int

    def __hash__(self):
        return hash((self.x, self.y))

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Point):
            return NotImplemented
        return self.__hash__() == other.__hash__()


def board_clustering(points: List[Point], threshold: float) -> List[Set[Point]]:
    points_length = len(points)
    clusters: List[Set[Point]] = list()

    for i in range(points_length):
        cluster = {points[i]}
        for j in range(i + 1, points_length):
            if _distance(points[i], points[j]) <= threshold:
                cluster.add(points[j])

        add_to_clusters = True
        for existing_cluster in clusters:
            if cluster.issubset(existing_cluster):
                add_to_clusters = False
                break

        if add_to_clusters:
            clusters.append(cluster)

    return clusters


def _distance(p1: Point, p2: Point) -> float:
    return abs(p1.x - p2.x) + abs(p1.y - p2.y)
