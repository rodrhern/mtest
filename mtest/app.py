from typing import List, Set

from fastapi import FastAPI

from mtest.board_clustering import Point, board_clustering

app = FastAPI()

ACTIVE_LOCATIONS = [(0, 1), (0, 2), (2, 3), (4, 3), (7, 3), (8, 5), (8, 7), (8, 8)]
ACTIVE_POINTS = [Point(x=e[0], y=e[1]) for e in ACTIVE_LOCATIONS]


@app.get("/", response_model=List[Set[Point]])
async def root(distance: int = 1):
    response = board_clustering(ACTIVE_POINTS, distance)
    return response
