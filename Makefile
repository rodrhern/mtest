isort:
	isort .

isort_check:
	isort . --check

black:
	black .

black_check:
	black . --check

lint:
	flake8 .

types:
	mypy .

format: isort black

test: types isort_check black_check lint
	pytest
