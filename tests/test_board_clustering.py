from mtest.board_clustering import Point, board_clustering

ACTIVE_LOCATIONS = [(0, 1), (0, 2), (2, 3), (4, 3), (7, 3), (8, 5), (8, 7), (8, 8)]
ACTIVE_POINTS = [Point(x=e[0], y=e[1]) for e in ACTIVE_LOCATIONS]


def test_board_clustering_threshold_1():
    threshold = 1
    clusters = [
        [(0, 1), (0, 2)],
        [(2, 3)],
        [(4, 3)],
        [(7, 3)],
        [(8, 5)],
        [(8, 7), (8, 8)],
    ]

    clustered_points = board_clustering(ACTIVE_POINTS, threshold)
    for i in range(len(clusters)):
        assert clustered_points[i] == {Point(x=e[0], y=e[1]) for e in clusters[i]}


def test_board_clustering_threshold_3():
    threshold = 3
    clusters = [
        [(0, 1), (0, 2)],
        [(0, 2), (2, 3)],
        [(2, 3), (4, 3)],
        [(4, 3), (7, 3)],
        [(7, 3), (8, 5)],
        [(8, 5), (8, 7), (8, 8)],
    ]

    clustered_points = board_clustering(ACTIVE_POINTS, threshold)
    for i in range(len(clusters)):
        assert clustered_points[i] == {Point(x=e[0], y=e[1]) for e in clusters[i]}


def test_board_clustering_threshold_5():
    threshold = 5
    clusters = [
        [(0, 1), (0, 2), (2, 3)],
        [(0, 2), (2, 3), (4, 3)],
        [(2, 3), (4, 3), (7, 3)],
        [(7, 3), (8, 5), (8, 7)],
        [(8, 5), (8, 7), (8, 8)],
    ]

    clustered_points = board_clustering(ACTIVE_POINTS, threshold)
    for i in range(len(clusters)):
        assert clustered_points[i] == {Point(x=e[0], y=e[1]) for e in clusters[i]}
