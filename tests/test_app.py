from fastapi.testclient import TestClient

from mtest.app import app

client = TestClient(app)


def test_app():
    threshold = 1
    expected_response = [
        [{"x": 0, "y": 1}, {"x": 0, "y": 2}],
        [{"x": 2, "y": 3}],
        [{"x": 4, "y": 3}],
        [{"x": 7, "y": 3}],
        [{"x": 8, "y": 5}],
        [{"x": 8, "y": 7}, {"x": 8, "y": 8}],
    ]

    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == expected_response

    response = client.get(f"/?distance={threshold}")
    assert response.status_code == 200
    assert response.json() == expected_response
